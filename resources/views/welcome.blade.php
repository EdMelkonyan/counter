<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Counter Task</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        $(document).ready(function(){
            $.ajax({url: "/getInc", success: function(data){
                    $("#counter").html(data.inc);
                }});
        });
    </script>
</head>
<body>

<p>X: {{$x}}</p>
<p>Y: {{$y}}</p>
<p>Prod: {{$x*$y}}</p>
<p>Rand: <span class="rand">{{$rand}}</span></p>
<p>Inc: <span id="counter"></span></p>
</body>
</html>
