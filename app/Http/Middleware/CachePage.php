<?php

namespace App\Http\Middleware;

use Closure;
use Cache;
use Queue;
use Storage;
use App\Jobs\Counter;
use HTMLDomParser;

class CachePage
{
    public function handle($request, Closure $next)
    {
        $key = $request->fullUrl();
        Queue::push(new Counter());

        $inc = \DB::table('counter')->first()->count;

        if (Cache::has($key)) {  //If cached
            $x = $request->x;
            $y = $request->y;

            $cachedHtml = Cache::get($key);
            $cachedRand = HTMLDomParser::str_get_html($cachedHtml)->find('span.rand')[0]->plaintext;

            $json = json_encode([
                'datetime' => date('Y-m-d H:i:s'),
                'x' => $x,
                'y' => $y,
                'prod' => $x*$y,
                'rand' => $cachedRand,
                'inc' => $inc
            ]);

            if (Storage::exists('list.json')){
                Storage::append('list.json', $json);
            }else{
                Storage::put('list.json', $json);
            }

            return response($cachedHtml);
        }
        $rand = rand(1, 100);

        $request->rand = $rand;
        $request->inc = $inc;

        $response = $next($request);  //If it wasn't cached, execute the request and grab the response


        Cache::put($key, $response->getContent(), $rand);

        return $response;
    }
}
