<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Storage;


class CounterController extends Controller
{
    public function index(Request $request)
    {

        $rand = $request->rand;
        $x = $request->x;
        $y = $request->y;

        $json = json_encode([
            'datetime' => date('Y-m-d H:i:s'),
            'x' => $x,
            'y' => $y,
            'prod' => $x*$y,
            'rand' => $rand,
            'inc' => $request->inc
        ]);

        if (Storage::exists('list.json')){
            Storage::append('list.json', $json);
        }else{
            Storage::put('list.json', $json);
        }


        return view('welcome', compact('x', 'y', 'rand'));
    }

    public function getCount(){
        $inc = DB::table('counter')->first()->count;
        return response()->json(['inc' => $inc]);
    }
}
